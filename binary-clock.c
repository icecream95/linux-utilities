#include <curses.h>
#include <time.h>
#include <unistd.h>

int h, w;
int ofh, ofw;

/* We have a 10x6 display:
   ----------
   --X--X--X-
   --X-XX-XX-
   -XX-XX-XX-
   -XX-XX-XX-
   ----------
*/

void print_d(int x, int y, int o)
{
     int i, j;
     char ch = '0';
     switch (o) {
     case 0:
          attrset(COLOR_PAIR(1));
          ch = '.';
          break;
     case 1:
          attrset(COLOR_PAIR(2));
          ch = 'X';
          break;
     default:
          attrset(COLOR_PAIR(0));
     }
     for (i = 0; i < h; ++i)
          mvhline(h*y+i+ofh, x*w+ofw, ch, w);
}

int main()
{
     time_t time_temp;
     struct tm cur_time;
     int oh = 1, ow = 1;
     int ah, aw;

     initscr();
     start_color();
     init_pair(2, COLOR_WHITE, COLOR_WHITE);
     init_pair(1, COLOR_BLUE, COLOR_BLUE);
     for (;;) {
          getmaxyx(stdscr,h,w);
          if (h != oh || w != ow)
               clear();
          oh = h;
          ow = w;
          ofh = (h%6)/2;
          ofw = (w%10)/2;
          h /= 6;
          w /= 10;
          if (h == 0)
               h = 1;
          if (w == 0)
               w = 1;
          time_temp = time(NULL);
          cur_time = *localtime(&time_temp);

          print_d(1,3,(cur_time.tm_hour >= 20)?1:0);
          print_d(1,4,(cur_time.tm_hour <20 && cur_time.tm_hour >=10)?1:0);

          print_d(2,1,((cur_time.tm_hour%10)&8)?1:0);
          print_d(2,2,((cur_time.tm_hour%10)&4)?1:0);
          print_d(2,3,((cur_time.tm_hour%10)&2)?1:0);
          print_d(2,4,((cur_time.tm_hour%10)&1)?1:0);

          print_d(4,2,((cur_time.tm_min/10)&4)?1:0);
          print_d(4,3,((cur_time.tm_min/10)&2)?1:0);
          print_d(4,4,((cur_time.tm_min/10)&1)?1:0);

          print_d(5,1,((cur_time.tm_min%10)&8)?1:0);
          print_d(5,2,((cur_time.tm_min%10)&4)?1:0);
          print_d(5,3,((cur_time.tm_min%10)&2)?1:0);
          print_d(5,4,((cur_time.tm_min%10)&1)?1:0);

          print_d(7,2,((cur_time.tm_sec/10)&4)?1:0);
          print_d(7,3,((cur_time.tm_sec/10)&2)?1:0);
          print_d(7,4,((cur_time.tm_sec/10)&1)?1:0);

          print_d(8,1,((cur_time.tm_sec%10)&8)?1:0);
          print_d(8,2,((cur_time.tm_sec%10)&4)?1:0);
          print_d(8,3,((cur_time.tm_sec%10)&2)?1:0);
          print_d(8,4,((cur_time.tm_sec%10)&1)?1:0);

          getmaxyx(stdscr,ah,aw);
          attrset(COLOR_PAIR(2));
          mvaddch(ah-1,aw-1,' ');
          refresh();
          sleep(1);
          attrset(COLOR_PAIR(0));
          mvaddch(ah-1,aw-1,' ');
     }
}
