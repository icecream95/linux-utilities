#!/usr/bin/env bash

if [ "$1" ]; then
    for target in "$@"
    do
        file="$(echo $target | rev | cut -d"." -f1 | rev)"
        if [ -f "${file}.compile" ]; then
            echo Compiling ${file%.compile}...
            bash $file.compile
        elif [ -f "${file}.cpp" ]; then
            echo Compiling ${file%.cpp}...
            g++ $file -o $( echo $file | head -c-5 )
        fi
        if [ -f "${file}.bashrc" ]
        then
            if ! [ -f /etc/profile.d/cli-utilities.sh ]
            then
                echo '[ "$SHELL" = "/bin/bash" ] || return' >/etc/profile.d/cli-utilities.sh
            fi
            if [ "$(cat /etc/profile.d/cli-utilities.sh | grep -n "# ${file}_INSTALLED_MARKER")" ]; then
                sed -i $(cat /etc/profile.d/cli-utilities.sh | grep -n "# ${file}_INSTALLED_MARKER" | cut -d':' -f1 | head -n2 | head -c-1 | tr "\n" ,)d /etc/profile.d/cli-utilities.sh
            fi
            echo Installing $file.bashrc into /etc/profile.d/cli-utilities.sh...
            echo "# ${file}_INSTALLED_MARKER" >>/etc/profile.d/cli-utilities.sh
            cat $file.bashrc >>/etc/profile.d/cli-utilities.sh
            echo "# ${file}_INSTALLED_MARKER" >>/etc/profile.d/cli-utilities.sh
        fi
    done
else
    for file in *.cpp
    do
        if ! [ -f "${file%cpp}compile" ]
        then
            echo Compiling ${file%.cpp}...
            g++ $file -o $( echo $file | head -c-5 )
        fi
    done
    for file in *.compile
    do
        echo Compiling ${file%.compile}...
        bash $file
    done
    echo '[ "$SHELL" = "/bin/bash" ] || return' >/etc/profile.d/cli-utilities.sh
    for file in $(ls | grep -v install.sh | grep -v ~$ )
    do
        if [ "$(file $file | grep executable)" ] || [ "$(file $file | grep 'shared object')" ]
        then
            echo Installing $file...
            if [ "$1" ]
            then
                install $file $1/$file
            else
                install $file /usr/local/bin/$file
            fi
            if [ -f "$file.bashrc" ]
            then
                echo Installing $file.bashrc into /etc/profile.d/cli-utilities.sh...
                echo "# ${file}_INSTALLED_MARKER" >>/etc/profile.d/cli-utilities.sh
                cat $file.bashrc >>/etc/profile.d/cli-utilities.sh
                echo "# ${file}_INSTALLED_MARKER" >>/etc/profile.d/cli-utilities.sh
            fi
        fi
    done
    source /etc/profile.d/cli-utilities.sh
fi
