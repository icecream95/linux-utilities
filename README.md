# linux-utilities

A collection of small utilities for Linux.

## Installing:

To install the utilities, run this:

    sudo ./install.sh [TARGETS]...

This will install the specified targets (defaults to everything) to
/usr/local/bin.

It will add lines to /etc/profile.d/cli-utilities.sh, so you might need to
source it in your ~/.bashrc.

The install script will automatically install all executable files except for
install.sh, and all .bashrc files with the same name as an executable file.

## Utilities:

### useful-command:

Useful-command is a command for storing shell commands so they can be quickly
run again.

    useful-command                    Adds the last command run to the useful-command list (will only work on bash)
    useful-command add '<COMMAND>'    Adds COMMAND to the useful-command list.
    useful-command list               Lists, numbered, all commands in the useful-command list.
    useful-command show <N>           Shows command N in the useful-command list.
    useful-command run <N>            Runs command N in the useful-command list.

The useful-command list is in ~/.useful-commands

Note that commands such as `cd` will (currently) not work, as this launches a
child shell to run commands. For some of these commmands, you can use
`$(useful-command show <N>)` instead.

### binary-clock:

binary-clock is an curses based binary clock, which is designed to be run from
the terminal. It uses colour and detects window resizes.

The first two columns are the hour in BCD format; the next minutes, and finally
seconds.

Use `^C` (control-c) to exit the clock.

### strip-control:

strip-control stips control characters from standard input and outputs to
standard output. Currently, it works for:

  * Cursor movement control characters (`^[[A` etc.). Note that this simply
    removes these characters.
  * Colour control characters (`^[[00m` etc.).

### progress:

Displays a progress bar.

    progress REGEX COUNT

For example, for installing a directory of Slackware packages you might do this:

    installpkg *.txz |& progress 'Package .* installed' $(ls *.txz | wc -l)
