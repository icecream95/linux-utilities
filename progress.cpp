#include <chrono>
#include <cstdlib>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

using namespace std;

int main(int argc, char* argv[])
{
    if (argc != 3) {
        cout << "USAGE: " << argv[0] << " NEXTREGEX COUNT\n";
        return 1;
    }
    regex r{argv[1],regex::optimize|regex::icase|regex::grep};
    // Comment the |regex::grep to use c++ regex.
    // I will soon add command-line options for doing this.
    stringstream str{argv[2]};
    int done{0};
    str >> done;
    int count{0};
    string s;
    auto time = chrono::steady_clock::now();
    int cols = system("exit $(/usr/bin/env tput cols)")/256;
    string ptmp = "\n[" + string((cols-2)*count/done,'#') +
                  string(cols-2-((cols-2)*count/done),' ') + ']';
    string tmp;
    string ctmp(cols,' ');
    while (getline(cin,s) && cin.good()) {
        if (regex_search(s, r)) {
            ++count;
            ptmp = "\n[" + string((cols-2)*count/done,'#') +
                   string(cols-2-((cols-2)*count/done),' ') + ']';
        }
        if (count > done)
            count = done;
        if (chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-time).count() > 2500) {
            int tcols = system("exit $(/usr/bin/env tput cols 2>/dev/null)")/256;
            // Is there a better way to work out the number of columns?
            time = chrono::steady_clock::now();
            if (tcols != cols) {
                ctmp = string(cols,' ');
                ptmp = "\n[" + string((cols-2)*count/done,'#') +
                       string(cols-2-((cols-2)*count/done),' ') + ']';
            }
            cols = tcols;
        }
        tmp = '\r' + ctmp + '\r' + s;
        cout << tmp + ptmp << flush;
    }
}
